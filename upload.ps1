$package_exist =(choco search fah --version 7.6.9 | Select -last 1)
if ($package_exist -eq '0 packages found.') { 
	Write-Host "Pushing the package"
	choco push fah.7.6.9.nupkg
} else {
	Write-Host "Package already available, skipping"
}